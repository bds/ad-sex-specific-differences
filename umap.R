library(Seurat)
library(patchwork)
plot_umap <- function(sobj_path, cell_name){
  
  sobj <- readRDS(sobj_path)
  sobj <- UpdateSeuratObject(sobj)
  P <- DimPlot(sobj, raster = F)
  rm(sobj)
  gc()
  return(P)
}

sobjects <- list.files("ad_knowledge_portal_syn52293433/rds_files/", full.names = T, pattern = "rds")
umap_plots <- list()
i <- 1
for (sobjpath in sobjects){
  tryCatch({
    unmap_plot <- plot_umap(sobjpath, tools::file_path_sans_ext(basename(sobjpath)))
    umap_plots[[i]] <- unmap_plot
    i <- i + 1
  }, error = function(cond){
    message(paste0("an error occured in", tools::file_path_sans_ext(basename(sobjpath))))
  })

}

p <- umap_plots[[1]] + umap_plots[[2]]  + umap_plots[[4]] + 
  umap_plots[[5]] + umap_plots[[3]] + plot_layout(ncol = 1)
p
ggsave(plot = p, filename = "ad_knowledge_portal_syn52293433/umap.jpeg",  width = 15, height = 15)
