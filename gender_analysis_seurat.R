library(Seurat) 
library(dplyr)
library(ggplot2)
# Map clinical data to the metadata 
merge_metadata <- function(sobj, metadata){
  
  meta_data_sex <- inner_join(sobj@meta.data, metadata)
  rownames(meta_data_sex) <- rownames(sobj@meta.data)
  sobj@meta.data <- meta_data_sex
  return(sobj)
}
# Assign the AD variable and process the metadata 
process_sobj_metadata <- function(sobj){
  
  sobj@meta.data <- sobj@meta.data %>%
    mutate(AD = ifelse(cogdx %in% c(4, 5), 1, ifelse(cogdx == 1, 0, NA))) #1 = Yes, 0 = No
  sobj_filt <- subset(sobj, subset = (AD == 0 | AD == 1))
  
  sobj_filt@meta.data$AD <- ifelse(sobj_filt@meta.data$AD == 0, "CTRL", "AD")
  sobj_filt@meta.data$msex <- ifelse(sobj_filt@meta.data$msex == 1, "M", "F")
  
  sobj_filt@meta.data$AD <- paste0(sobj_filt@meta.data$AD, "_", sobj_filt@meta.data$msex)
  sobj_filt@meta.data$cell_type_high_resolution <- make.names(sobj_filt@meta.data$cell_type_high_resolution)
  
  
  if (length(unique(sobj_filt@meta.data$cell_type_high_resolution)) > 1)
  {
    sobj_filt@meta.data$cell_type_high_resolution <- sapply(strsplit(sobj_filt@meta.data$cell_type_high_resolution, "\\."), "[", 1)
  }
  sobj_filt@meta.data$group <- paste0(sobj_filt@meta.data$cell_type_high_resolution, "_", sobj_filt@meta.data$AD)
  
  cell_idents <- as.factor(sobj_filt@meta.data$group)
  names(cell_idents) <- names(sobj_filt@active.ident)
  sobj_filt@active.ident <- cell_idents
  return(sobj_filt)
}

# Determining gender-specific and gender-dimorphic DEGs using adjusted significance and a nominal p-value specificity filter
# a minimum absolute logFC threshold for gender-dimorphic genes + min. abs. logFC for the target gender for gender-specific genes
# (still consider as gender-shared if significant in both genders with shared logFC, and abs logFC only above 0.25 in one gender)
gender_spec_genes <- function(df_M, df_F, target_fdr = 0.05, exclude_pval = 0.5, minabslog=0.25){
  
  # filter by target gene FDR
  male_canddeg = rownames(df_M)[which(df_M$p_val_adj < target_fdr)]
  
  # filter other gender non-significance (and not close to significance) nominal p-value threshold
  male_spec_genes = male_canddeg[which(df_F[match(male_canddeg, rownames(df_F)),]$p_val > exclude_pval)]
  
  # filter by abs. logFc threshold in target gender
  male_spec_genes = male_spec_genes[which(abs(df_M[match(male_spec_genes, rownames(df_M)),]$avg_log2FC) >minabslog)]
  
  
  print("Male-specific genes:")
  print(male_spec_genes)
  
  female_canddeg = rownames(df_F)[which(df_F$p_val_adj < target_fdr)]
  
  female_spec_genes = female_canddeg[which(df_M[match(female_canddeg, rownames(df_M)),]$p_val > exclude_pval)]
  
  # filter by abs. logFc threshold in target gender
  female_spec_genes = female_spec_genes[which(abs(df_F[match(female_spec_genes, rownames(df_F)),]$avg_log2FC) >minabslog)]	
  
  print("Female-specific genes:")
  print(female_spec_genes)
  
  # shared DEGs == gender-dimorphic
  
  dimorphic_genes = NULL
  intdegs = intersect(male_canddeg, female_canddeg)
  
  if(length(intdegs)){
    # different sign of the fold-change
    logfcs_male = df_M[match(intdegs, rownames(df_M)),]$avg_log2FC
    logfcs_female = df_F[match(intdegs, rownames(df_F)),]$avg_log2FC	
    
    diff_fc = intersect(which(sign(logfcs_male) != sign(logfcs_female)), intersect(which(abs(logfcs_male)>minabslog), which(abs(logfcs_female)>minabslog)))
    dimorphic_genes = intdegs[diff_fc]	
    
    shared_fc = which(sign(logfcs_male) == sign(logfcs_female))
    # optionally add: minabslog fulfilled in at least one of the genders
    shared_genes = intdegs[shared_fc]		
  }
  
  print("Gender-dimorphic genes:")
  print(dimorphic_genes)
  
  print("Gender-shared genes:")
  print(shared_genes)
  
  dfres = data.frame("DEG type"=c(rep("male-specific",length(male_spec_genes)), rep("female-specific",length(female_spec_genes)), rep("gender-dimorphic",length(dimorphic_genes)), rep("gender-shared",length(shared_genes))), "Gene symbols"=c(male_spec_genes, female_spec_genes, dimorphic_genes, shared_genes), "Male avg. logFC"=c(df_M[match(male_spec_genes, rownames(df_M)),]$avg_log2FC, df_M[match(female_spec_genes, rownames(df_M)),]$avg_log2FC, df_M[match(dimorphic_genes, rownames(df_M)),]$avg_log2FC, df_M[match(shared_genes, rownames(df_M)),]$avg_log2FC), "Female avg. logFC"=c(df_F[match(male_spec_genes, rownames(df_F)),]$avg_log2FC, df_F[match(female_spec_genes, rownames(df_F)),]$avg_log2FC, df_F[match(dimorphic_genes, rownames(df_F)),]$avg_log2FC, df_F[match(shared_genes, rownames(df_F)),]$avg_log2FC), "Male FDR"=c(df_M[match(male_spec_genes, rownames(df_M)),]$p_val_adj, df_M[match(female_spec_genes, rownames(df_M)),]$p_val_adj, df_M[match(dimorphic_genes, rownames(df_M)),]$p_val_adj, df_M[match(shared_genes, rownames(df_M)),]$p_val_adj), "Female FDR"=c(df_F[match(male_spec_genes, rownames(df_F)),]$p_val_adj, df_F[match(female_spec_genes, rownames(df_F)),]$p_val_adj, df_F[match(dimorphic_genes, rownames(df_F)),]$p_val_adj, df_F[match(shared_genes, rownames(df_F)),]$p_val_adj))
  
  return(dfres)
  
}


# Main run 
sobjects <- list.files(path = "ad_knowledge_portal_syn52293433/rds_files/", pattern = "rds", full.names = T)
metadata <- read.csv("ad_knowledge_portal_syn52293433/ROSMAP_clinical.csv") #1 = Male, 0 = Female


outpath <- "ad_knowledge_portal_syn52293433/new-analysis/" 
options(future.globals.maxSize = 9991289600)

for (sobj_path in sobjects){
  
  tryCatch({
    
    sobj <- readRDS(sobj_path)
    sobj <- UpdateSeuratObject(sobj)
    # Pre process the data 
    sobj <- merge_metadata(sobj, metadata)
    sobj_filt <- process_sobj_metadata(sobj)
    
    
    if (length(unique(sobj_filt@active.ident)) > 1){
      cell_name <- tools::file_path_sans_ext(basename(sobj_path))
      
      # Create a directory for output
      output_path <- paste0(outpath, cell_name, "/")
      # Make an output directory
      dir.create(path = output_path)
      groups <- unique(sapply(strsplit(as.character(unique(Idents(sobj_filt))), "_"), "[", 1))
      for (group in groups) {
        
        group_name <- paste0(group, "_AD_M")
        control_name <- paste0(group, "_CTRL_M")
        
        male_poisson <- FindMarkers(sobj_filt, ident.1 = group_name, ident.2 = control_name, 
                                    verbose = FALSE, test.use = "poisson", assay="RNA",
                                    only.pos = FALSE, logfc.threshold = -Inf)
        
        group_name <- paste0(group, "_AD_F")
        control_name <- paste0(group, "_CTRL_F")
        
        female_poisson <- FindMarkers(sobj_filt, ident.1 = group_name, ident.2 = control_name, 
                                      verbose = FALSE, test.use = "poisson", assay="RNA",
                                      only.pos = FALSE, logfc.threshold = -Inf)
        
        sex_related_genes <- gender_spec_genes(df_M = male_poisson, df_F = female_poisson)
        group_file_name <- paste0(group, "_M_F_difference.csv")
        write.csv(sex_related_genes, file = paste0(output_path, group_file_name))
      }
      
      
    }else{
      message(paste0("Class type: ", cell_name, " has no subtypes"))
      next
    }
  }, 
    error = function(cond){
      message("An error occured.")
      message(cond)
      })
}


