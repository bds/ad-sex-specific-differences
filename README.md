### Replication of Sex-Dependent Alterations of Apoptotic Processes in Alzheimer's Disease Revealed by Large-Scale Single-Cell Transcriptomics

- `gender_analysis_seurat.R` is used to perform the differential expression analysis using Seurat (version 5.1.0, RRID:SCR_016341)
- `biological_analysis.R`is used to visualize the extracted DEGs using clusterProfiler (version 4.12.0 RRID: SCR_016884), KEGG (RRID: SCR_018145), and Gene Ontology (RRID: SCR_010326) 
- `cell_cell_comm.R` is used to perform cell-cell communication analysis using NichNetR (version 2.1.5), and CellChatDB (version 1.6.1)